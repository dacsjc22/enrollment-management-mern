### `Project Details`
    This is a small and simple enrollment management project, that let you manage students details, enrollments, and course. 

### `Scope of the project `
    - has a Cloud Database connected (MongoDB Atlas)
    - user can view, create, delete, and update student details and course.
    - user can enroll student to multiple courses.
    
### ` Limitation `
    - cannot remove the student from the course registered.
    - requires to run the react js and backend server in terminal


### `prerequisite`
    This app needs a latest version of node.js

### `Installation`
    -> Open your terminal or command line and go to the student_course_management directory.
    -> Once your in the directory, type "npm install" (this will install all node_modules).
    -> After the npm installation is doen. Go to the "backend" folder inside the directory, and type "npm install".
    That's all needed to be install in the project.

### `Running Backend Server`
    In this project you need to open two terminal, one for the react js and one for the backend.
    
    *Starting React JS
    -> Open your terminal or command line then go "student_course_management" directory.
    -> Type "npm start" to start the react js application.

    *Starting Backend 
    -> Open your terminal or command line then go  to the folder named "backend" in "student_course_management" directory.
    -> Once your inside the folder "backend" type npm start.
    (DONT CLOSE THE TERMINAL WHILE ITS RUNNING)


### `Support`
    This project is open for improvement!
    If any problems occurs or things that you would like to suggest contact me by my email justincarldacanay@gmail.com . 
    Thank you!
