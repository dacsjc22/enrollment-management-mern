const express = require('express');
//const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true}
    );
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully")
});

const courseRouter = require('./routes/course.router');
const studentRouter = require('./routes/student.router');
const enrollmentRouter = require('./routes/enrollment.router');

app.use('/courses', courseRouter);
app.use('/students', studentRouter);
app.use('/enrollment', enrollmentRouter);
app.listen(port, ()=>{
    console.log(`Server is running in port: ${port}`);
});