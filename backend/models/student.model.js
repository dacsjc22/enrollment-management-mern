const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const studentSchema = new Schema({
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
    middle_name: {
        type: String,
    },
    birthdate: {
        type: Date,
        required: true,
    },
    gender: {
        type: String,
        required: true,
    }
},{
    timestamps: true,
});

const User = mongoose.model('Student', studentSchema);

module.exports = User;