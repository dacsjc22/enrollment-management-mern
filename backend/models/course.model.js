const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const childStudentSchema = new Schema({
    student_id : String,
    first_name: String,
    last_name: String,
    middle_name: String,
    dateCreated: Date,
});
const courseSchema = new Schema({
    course_name: {
        type: String,
        required: true,
    },
    image_path: {
        type: String,
    },
    students: [childStudentSchema]
},{
    timestamps: true,
});

const Course = mongoose.model('Course', courseSchema);

module.exports = Course;