const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const courseSchema = new Schema({
    course_id: {
        type: String,
        required: true,
    },
    student_id: {
        type: String,
        required: true,
    },
    first_name: {
        type: String,
        required: true,
    },
    middle_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
},{
    timestamps: true,
});

const CourseStudent = mongoose.model('Course_Student', courseSchema);

module.exports = CourseStudent;