const router = require('express').Router();
let Enrollment = require('../models/course.model');
let Student = require ('../models/student.model');


router.route('/:id').get((req, res) => {
    Enrollment.findById(req.params.id)
        .then(course => res.json(course.students))
        .catch(err => res.status(400).json('Error: ' + err));
});
router.route('/add/:id').post((req, res) => {
    const student_id = req.body.student_id;

    Student.findById(student_id)
    .then(student => {
 
        const first_name = student.first_name;
        const last_name = student.last_name;
        const middle_name = student.middle_name;
        const dateCreated = new Date(); 
        const newEnrollment ={
            student_id,
            first_name,
            last_name,
            middle_name,
            dateCreated
        };
        Enrollment.findById(req.params.id)
        .then(course => {
            course.students[course.students.length] =  newEnrollment;
    
            course.save()
                .then(() => res.json('Enrollment updated!'))
                .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));

    }).catch(err => res.status(400).json('Error: ' + err));


});


module.exports = router; 