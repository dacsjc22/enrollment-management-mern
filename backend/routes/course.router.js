const router = require('express').Router();
const lodash = require('lodash');
let Course = require('../models/course.model');

router.route('/').get((req, res)=>{
    Course.find()
        .then(course => res.json(course))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    const course_name = req.body.course_name;
    let image_path = req.body.image_path;

    if(lodash.isEmpty(req.body.image_path)){
        image_path = 'https://via.placeholder.com/600x300/?text='+ course_name.replace(/\s/g, "+");
    }

    const newCourse = new Course({
        course_name,
        image_path,
    });
    newCourse.save()
        .then(() => res.json('Course added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
    Course.findById(req.params.id)
        .then(course => res.json(course))
        .catch(err => res.status(400).json('Error: ' + err));
});
router.route('/:id').delete((req, res) => {
    Course.findByIdAndDelete(req.params.id)
        .then(course => res.json('Course deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});
router.route('/update/:id').post((req, res) => {
    Course.findById(req.params.id)
        .then(course => {
            course.course_name = req.body.course_name;
            course.image_path = req.body.image_path;

            course.save()
                .then(() => res.json('Course updated!'))
                .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router; 