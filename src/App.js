import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css"

import Navbar from "./components/navbar/navbar.component"
import StudentList from "./components/student/student-list.component";
import StudentCreate from "./components/student/student-create.component";
import StudentEdit from "./components/student/student-edit.component";

import CoursesList from "./components/courses/courses-list.component";
import CoursesCreate from "./components/courses/courses-create.component";
import CoursesEdit from "./components/courses/courses-edit.component";

import EnrollmentList from "./components/enrollment/enrollment-list.component";
function App() {
  return (
    <Router>
    <Navbar />
      <div className="container">
        <Route path="/" exact component={StudentList} />
        <Route path="/student" exact component={StudentList} />
        <Route path="/student/create" component={StudentCreate} />
        <Route path="/student/edit/:id" component={StudentEdit} />
        <Route path="/courses" exact component={CoursesList} />
        <Route path="/courses/create" component={CoursesCreate}/>
        <Route path="/courses/edit/:id" component={CoursesEdit} />
        <Route path="/courses/enrollment/edit/:id" component={EnrollmentList} />
      </div>
    </Router>
  );
}

export default App;
