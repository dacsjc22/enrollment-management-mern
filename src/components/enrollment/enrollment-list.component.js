import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
const Students = props => (
    <tr>
        <td>{props.students.first_name} {props.students.middle_name} {props.students.last_name}</td>
        <td>{props.students.dateCreated.substring(0,10)}</td>
    </tr>
)

const AddStudent = props => (
    <option value={props.students.student_id}>{props.students.first_name} {props.students.middle_name} {props.students.last_name}</option>
);
export default class EnrollmentList extends Component {
    constructor(props){
        super(props);

        this.deleteStudent = this.deleteStudent.bind(this);
        this.onChangeSelectStudent = this.onChangeSelectStudent.bind(this);
        this.onSubmit = this.onSubmit.bind(this)
        this.state = {
            students: [], 
            select_student:[], 
            student_id: ''
        }
    }
    onChangeSelectStudent(e){
        this.setState({
            student_id: e.target.value
        });
        console.log(this.state.student_id);
    }
    componentDidMount(){
        axios.get('http://localhost:5000/enrollment/'+this.props.match.params.id)
        .then(response=>{
            this.setState({
                students: response.data
            })
        }).catch((error) => {
            console.log(error);
        });

        axios.get('http://localhost:5000/students/')
        .then(response=>{
            var student_choice = response.data;
            console.log(student_choice);
            this.state.students.forEach(element => {
                student_choice = student_choice.filter(el => el._id !== element.student_id);
            });
            this.setState({
                select_student : student_choice
            });
        })
    }
    deleteStudent(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                axios.delete(`http://localhost:5000/students/${id}`).then(response => this.successResponse('Successfully deleted!')).catch((error)=>this.errorResponse(error));
                this.setState({
                    students: this.state.students.filter(el => el._id !== id)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel){
              Swal.fire(
                'Cancelled',
                'The data is safe :)',
                'error'
              )
            }
        })
    }

    successResponse(message){
        Swal.fire({
            icon: 'success',
            title: 'Successfully added!',
        })
    }

    errorResponse(data){
        console.log(data);
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
        });
    }

    studentList(){
        return this.state.students.map(currentStudents => {
            return <Students students={currentStudents} key={currentStudents._id}/>;
        })
    }
    addStudent(){
        return this.state.select_student.map(currentStudents => {
            return <AddStudent students={currentStudents} key={currentStudents._id}/>;
        })
    }
    onSubmit(e){
        e.preventDefault();
        console.log(this.state.student_id);

        const student = {
            student_id: this.state.student_id
        };
        axios.post('http://localhost:5000/enrollment/add/'+this.props.match.params.id, student).then(response => {
            this.successResponse(response)
            axios.get('http://localhost:5000/enrollment/'+this.props.match.params.id)
            .then(response=>{
                this.setState({
                    students: response.data
                })
            }).catch((error) => {
                console.log(error);
            });
    
            axios.get('http://localhost:5000/students/')
            .then(response=>{
                var student_choice = response.data;
                console.log(student_choice);
                this.state.students.forEach(element => {
                    student_choice = student_choice.filter(el => el._id !== element.student_id);
                });
                this.setState({
                    select_student : student_choice
                });
            })
            this.setState({
                student_id: ''
            })
        }, error=>this.errorResponse(error));
        

    }
    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Enrollment list
                            </h3>
                        </div>
                        <div className="card-body">
                            <table className="table">
                                <thead className="table-dark">
                                    <tr>
                                        <th>Student Name</th>
                                        <th>Date Enrolled</th>
                                    </tr>
                                </thead>
                                <tbody onUpdate={this.onSubmit}>
                                    { this.studentList()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Enroll student
                            </h3>
                        </div>
                        <form onSubmit={this.onSubmit}>
                            <div className="card-body">
                                <div className="form-group">
                                    <select className="form-control" value={this.state.student_id} onChange={this.onChangeSelectStudent} onUpdate={this.onSubmit}required>
                                        <option value="" disabled>Select a student</option>
                                        { 
                                            this.state.select_student.map(function(student){
                                                return <option key={student._id} value={student._id}>{student.first_name} {student.middle_name} {student.last_name}</option>;
                                            }) 
                                        }
                                    </select>   
                                </div>
                                <div className="form-group float-right">
                                    <button className="btn btn-primary" data-toggle="modal"> Add student</button>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        );
    }
}