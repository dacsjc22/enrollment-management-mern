import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
const Courses = props => (
    <tr>
        <td>{props.courses.course_name}</td>
        <td>{props.courses.createdAt.substring(0,10)}</td>
        <td><Link to={"/courses/enrollment/edit/"+props.courses._id}>manage enrollment</Link> | <Link to={"/courses/edit/"+props.courses._id}>edit</Link> | <a href="#" onClick={()=> { props.deleteCourse(props.courses._id)}}>delete</a> </td>
    </tr>
)
export default class CoursesList extends Component {
    constructor(props){
        super(props);

        this.deleteCourse = this.deleteCourse.bind(this);
        this.state = {courses: []}
    }
    componentDidMount(){
        axios.get('http://localhost:5000/courses/')
        .then(response=>{
            this.setState({
                courses: response.data
            })
        }).catch((error) => {
            console.log(error);
        });
    }
    deleteCourse(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                axios.delete(`http://localhost:5000/courses/${id}`).then(response => this.successResponse('Successfully deleted!')).catch((error)=>this.errorResponse(error));
                this.setState({
                    courses: this.state.courses.filter(el => el._id !== id)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel){
              Swal.fire(
                'Cancelled',
                'The data is safe :)',
                'error'
              )
            }
        })
    }

    successResponse(message){
        Swal.fire({
            icon: 'success',
            title: message,
        })
    }

    errorResponse(data){
        console.log(data);
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
        });
    }

    courseList(){
        return this.state.courses.map(currentCourses => {
            return <Courses courses={currentCourses} deleteCourse={this.deleteCourse} key={currentCourses._id}/>;
        })
    }
    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Course List
                            </h3>
                        </div>
                        <div className="card-body">
                            <div className="form-group float-right">
                                <Link to="/courses/create"><button className="btn btn-primary">Create course</button></Link>
                            </div>
                            <table className="table">
                                <thead className="table-dark">
                                    <tr>
                                        <th>Course Name</th>
                                        <th>Date Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { this.courseList()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}