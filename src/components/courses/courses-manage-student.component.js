import React, { Component } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2'

export default class CoursesManageStudent extends Component {
    constructor(props) {
        super(props);

        this.onChangeCourseName = this.onChangeCourseName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            course_name: '',
        }
    }
    componentDidMount() {
        axios.get('http://localhost:5000/users/')
        .then(response=>{
            this.setState({
                students: response.data.map(student => {
                    return student.first_name + ' '+ student.middle_name + ' ' + student.last_name;
                }),
                student: (response.data[0].first_name + ' '+ response.data[0].middle_name + ' ' + response.data[0].last_name)
            });
        });
    }
    onChangeCourseName(e){
        this.setState({
            course_name: e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();

        const courses = {
            course_name: this.state.course_name
        }

        axios.post('http://localhost:5000/courses/add', courses).then(response => this.successResponse(response), error=>this.errorResponse(error));

        this.setState({
            course_name: ''
        })
    }
    successResponse(data){
        Swal.fire({
            icon: 'success',
            title: 'Successfully Created!',
        })
    }
    errorResponse(data){
        console.log(data);
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
        });
    }
    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Create Course
                            </h3>
                        </div>
                        <div className="card-body">

                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label>
                                    Course name:
                                </label>
                                <input type="text" className="form-control" value={this.state.course_name} onChange={this.onChangeCourseName} required></input>
                            </div>
                            <div className="form-group float-right">
                                <input type="submit" value="Create course" className="btn btn-primary"></input>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}