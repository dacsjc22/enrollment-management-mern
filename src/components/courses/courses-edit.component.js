import React, { Component } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2'; 
export default class CoursesEdit extends Component {
    constructor(props) {
        super(props);

        this.onChangeCourseName = this.onChangeCourseName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            course_name: '',
            image_path: '',
        }
    }
    componentDidMount(){
        axios.get('http://localhost:5000/courses/'+this.props.match.params.id)
        .then(response => {
            if(response.data){
                this.setState({
                    course_name: response.data.course_name,
                    image_path: response.data.image_path,
                })
            }
        }).catch((error)=>this.successResponse(error));

    }
    onChangeCourseName(e){
        this.setState({
            course_name: e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();

        const courses = {
            course_name: this.state.course_name,
            image_path: this.state.image_path
        }
        axios.post('http://localhost:5000/courses/update/'+this.props.match.params.id, courses).then(response => this.successResponse(response), error=>this.errorResponse(error));

    }

    successResponse(data){
        Swal.fire({
            icon: 'success',
            title: 'Successfully Updated!',
        })
    }

    errorResponse(data){
        console.log(data);
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
        });
    }
    
    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Edit Course
                            </h3>
                        </div>
                        <div className="card-body">

                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label>
                                    Course name:
                                </label>
                                <input type="text" className="form-control" value={this.state.course_name} onChange={this.onChangeCourseName} required></input>
                            </div>
                            <div className="form-group float-right">
                                <input type="submit" value="Edit course" className="btn btn-primary"></input>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
