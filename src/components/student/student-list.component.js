import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
const Students = props => (
    <tr>
        <td>{props.students.first_name} {props.students.middle_name} {props.students.last_name}</td>
        <td>{props.students.birthdate.substring(0,10)}</td>
        <td>{props.students.gender}</td>
        <td>{props.students.createdAt.substring(0,10)}</td>
        <td><Link to={"/student/edit/"+props.students._id}>edit</Link> | <a href="#" onClick={()=> { props.deleteStudent(props.students._id)}}>delete</a> </td>
    </tr>
)
export default class StudentList extends Component {
    constructor(props){
        super(props);

        this.deleteStudent = this.deleteStudent.bind(this);
        this.state = {students: []}
    }
    componentDidMount(){
        axios.get('http://localhost:5000/students/')
        .then(response=>{
            this.setState({
                students: response.data
            })
        }).catch((error) => {
            console.log(error);
        });
    }
    deleteStudent(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                axios.delete(`http://localhost:5000/students/${id}`).then(response => this.successResponse('Successfully deleted!')).catch((error)=>this.errorResponse(error));
                this.setState({
                    students: this.state.students.filter(el => el._id !== id)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel){
              Swal.fire(
                'Cancelled',
                'The data is safe :)',
                'error'
              )
            }
        })
    }

    successResponse(message){
        Swal.fire({
            icon: 'success',
            title: message,
        })
    }

    errorResponse(data){
        console.log(data);
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
        });
    }

    studentList(){
        return this.state.students.map(currentStudents => {
            return <Students students={currentStudents} deleteStudent={this.deleteStudent} key={currentStudents._id}/>;
        })
    }
    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Student List
                            </h3>
                        </div>
                        <div className="card-body">
                            <div className="form-group float-right">
                                <Link to="/student/create"><button className="btn btn-primary">Create student</button></Link>
                            </div>
                            <table className="table">
                                <thead className="table-dark">
                                    <tr>
                                        <th>Student Name</th>
                                        <th>Birthdate</th>
                                        <th>Gender</th>
                                        <th>Date Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { this.studentList()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}