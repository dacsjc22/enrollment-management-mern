import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import Swal from 'sweetalert2'

export default class StudentEdit extends Component {
    constructor(props) {
        super(props);

        this.onChangeFirstname = this.onChangeFirstname.bind(this);
        this.onChangeMiddlename = this.onChangeMiddlename.bind(this);
        this.onChangeLastname = this.onChangeLastname.bind(this);
        this.onChangeBirthdate = this.onChangeBirthdate.bind(this);
        this.onChangeGender = this.onChangeGender.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            first_name: '',
            last_name: '',
            middle_name: '',
            birthdate: new Date(),
            gender: '',
        }
    }

    onChangeFirstname(e){
        this.setState({
            first_name: e.target.value
        });
    }
    
    onChangeMiddlename(e){
        this.setState({
            middle_name: e.target.value
        });
    }

    onChangeLastname(e){
        this.setState({
            last_name: e.target.value
        });
    }

    onChangeBirthdate(e){
        this.setState({
            birthdate: e
        });
    }

    onChangeGender(e){
        this.setState({
            gender: e.target.value
        })
    }

    componentDidMount(){
        axios.get('http://localhost:5000/students/'+this.props.match.params.id)
        .then(response => {
            if(response.data){
                this.setState({
                    first_name: response.data.first_name,
                    middle_name: response.data.middle_name,
                    last_name: response.data.last_name,
                    birthdate: new Date(response.data.birthdate),
                    gender: response.data.gender,
                })
            }
        }).catch((error)=>this.successResponse(error));

    }
    onSubmit(e){
        e.preventDefault();

        const student = {
            first_name: this.state.first_name,
            middle_name: this.state.middle_name,
            last_name: this.state.last_name,
            birthdate: this.state.birthdate,
            gender: this.state.gender,
        }

        axios.post('http://localhost:5000/students/update/'+this.props.match.params.id, student).then(response => this.successResponse(response), error=>this.errorResponse(error));

  
    }

    successResponse(data){
        Swal.fire({
            icon: 'success',
            title: 'Successfully Updated!',
        })
    }

    errorResponse(data){
        console.log(data);
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
        });
    }

    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <h3>
                                Edit Student
                            </h3>
                        </div>
                        <div className="card-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>
                                        First name:
                                    </label>
                                    <input type="text" className="form-control" value={this.state.first_name} onChange={this.onChangeFirstname} required></input>
                                </div>
                                <div className="form-group">
                                    <label>
                                        Middle name:
                                    </label>
                                    <input type="text" className="form-control" value={this.state.middle_name} onChange={this.onChangeMiddlename}></input>
                                </div>
                                <div className="form-group">
                                    <label>
                                        Last name:
                                    </label>
                                    <input type="text" className="form-control" value={this.state.last_name} onChange={this.onChangeLastname} required></input>
                                </div>
                                <div className="form-group">
                                    <label>
                                        Birthdate:
                                    </label>
                                    <br></br>
                                    <DatePicker className="form-control"
                                        selected={this.state.birthdate}
                                        onChange={this.onChangeBirthdate}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>
                                        Gender:
                                    </label>
                                    <select className="form-control" value={this.state.gender} onChange={this.onChangeGender} required>
                                        <option value="" disabled>
                                            Select gender
                                        </option>
                                        <option value="male">
                                            Male
                                        </option>
                                        <option value="female">
                                            Female
                                        </option>
                                    </select>
                                </div>
                                <div className="form-group float-right">
                                    <input type="submit" value="Save" className="btn btn-primary btn-lg"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}