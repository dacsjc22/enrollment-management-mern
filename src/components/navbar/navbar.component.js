import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './navbar.component.css';

export default class Navbar extends Component {
    render () {
        return (
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg mb-5">
                <Link to="/" className="navbar-brand">Student Course Management</Link>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar-item">
                            <Link to="/" className="nav-link">
                                Student
                            </Link>
                        </li>

                        <li className="navbar-item">
                            <Link to="/courses" className="nav-link">
                                Courses
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}